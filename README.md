# CASTLE GTW Module
GTW module which utilizes the Spring Cloud Netflix libraries (http://cloud.spring.io/spring-cloud-netflix/spring-cloud-netflix.html).

## API Documentation
Autogenerated Swagger endpoint: Swagger http://localhost:9091/swagger-ui.html
http://localhost:9091/v3/api-docs/


## Resilience
https://resilience4j.readme.io/docs/getting-started-3




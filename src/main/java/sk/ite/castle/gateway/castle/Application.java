package sk.ite.castle.gateway.castle;

import io.github.resilience4j.common.retry.configuration.RetryConfigCustomizer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

import java.time.Duration;

@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication
public class Application {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public RetryConfigCustomizer myRetryConfiguration() {
		return RetryConfigCustomizer
				.of("global_retry", builder -> builder.retryExceptions( java.net.ConnectException.class, RuntimeException.class, Exception.class )
						.maxAttempts(3)
  				        .waitDuration(Duration.ofMillis(1000)));
	}
}

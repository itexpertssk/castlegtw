package sk.ite.castle.gateway.castle.application.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import sk.ite.castle.gateway.castle.application.dto.DTOCastle;

import java.util.List;

/**
 * Castle backend service client implemented using Netflix Feign client library.
 *
 */

@FeignClient("castle")
public interface CastleReader {
	@RequestMapping(method = RequestMethod.GET, value = "/castles")
    List<DTOCastle> getAllCastles();

}




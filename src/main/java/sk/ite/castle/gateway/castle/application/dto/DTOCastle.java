package sk.ite.castle.gateway.castle.application.dto;

/**
 * This class models a Castle data structure.
 * 
 * @author macalak@itexperts.sk
 *
 */

public class DTOCastle {
	public Long id;
	public String name;
	public String ruler;
	public String location;


	@Override
	public String toString() {
		return "DTOCastle{" +
				"id=" + id +
				", name='" + name + '\'' +
				", ruler='" + ruler + '\'' +
				", location='" + location + '\'' +
				'}';
	}
}

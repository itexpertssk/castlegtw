package sk.ite.castle.gateway.castle.application.restapi;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sk.ite.castle.gateway.castle.application.dto.DTOCastle;
import sk.ite.castle.gateway.castle.application.service.CastleReader;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/castles")
class CastleApiGateway {
	private static final Logger LOG= LoggerFactory.getLogger(CastleApiGateway.class);

	@Autowired
	private CastleReader castleReader;

	public List<DTOCastle> fallback() {
		List<DTOCastle> fb = new ArrayList<DTOCastle>();
		fb.add(new DTOCastle());
		return fb;
	}

	@RequestMapping(produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	@CircuitBreaker(name = "CircuitBreakerService")
	@Retry(name = "global_retry", fallbackMethod = "fallback")
	public List<DTOCastle> getAllCastles() {
		LOG.info("Getting castles ...");
		return (List<DTOCastle>) castleReader.getAllCastles();
	}
}
